FROM python:3.7.2-slim
MAINTAINER Ülgen Sarıkavak <work@ulgens.me>

# Install packages
RUN apt-get update && apt-get install \
        build-essential \
        libpq-dev \
        libgdal-dev \
        supervisor \
        libjpeg-dev \
        zlib1g-dev \
        libtiff-dev \
        libwebp-dev \
        libfreetype6-dev -y && \
    # Clean
    apt-get autoclean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
